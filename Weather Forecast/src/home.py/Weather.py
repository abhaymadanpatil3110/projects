from tkinter import *;
import tkinter as tk
from geopy.geocoders import Nominatim    #errcause
from tkinter import ttk,messagebox
from timezonefinder import TimezoneFinder
from datetime import *;
import requests;
import pytz;
from PIL import Image,ImageTk;


root = Tk();
root.title("Weather App");
root.geometry("890x470+300+300");
root.configure(bg = "#57adff");
root.resizable(False,False)

def getWeather():
    city = textfield.get();
    location = geolocator.geocode(city);

    geolocator = Nominatim(user_agent = "geoapiExercises");
    obj = TimezoneFinder()
    result = obj.timezone_at(lng = location.longitude,lat = location.latitude); #errcause
    long_lat.config(text = f"{location.longitude}{location.latitude}")
    timezone.config(text = result);
    long_lat.config(text=f"{round(location.latitude,4)}°N,{round(location.longitude)}°E");

    home_ = pytz.timezone(result);
    local_time = datetime.now(home_);
    current_time = local_time.strftime("%I:%M %p");
    clock.config(text = current_time);

    #weather
    api = "https://api.openweathermap.org/data/2.5/onecall?lat="+str(location.latitude)+"&lon="+str(location.longitude)+"&units=metric&exclude=hourly,&appid=bc495ac36dbf939483e2a9c2ebfb4644";
    json_data = requests.get(api).json()
    humidity = json_data['current']['humidity']
    pressure = json_data['current']['pressure']
    wind = json_data['current']['wind_speed']
    description = json_data['current']['weather'][0]['description']
    
    t.config(text=(temp,"°C"));
    t2.config(text=(humidity,"%"));
    t3.config(text = (pressure,"hPa"));
    t4.config(text = (wind,"m/s"));
    t5.config(text =(description));

    #current
    temp = json_data['current'][temp];  #KeyError will happen sometime
    print(temp)

    


##icon

image_icon = PhotoImage(file = "Image");
root.iconphoto(False,image_icon);   #Afer this image will be added to display
Round_box = PhotoImage(file = "Image_of_round from assets")
Label(root,iamge = Round_box,bg = "#57adff").place(x = 30,y = 110)

#Label

label1 = Label(root,text = "Temperature",font = ("Helvetica",11),fg = "white",bg = "#203243");
label1.place(x = 50,y = 120);

label2 = Label(root,text = "Humidity",font = ("Helvetica",11),fg = "white",bg = "#203243");
label2.place(x = 50,y = 140);

label3 = Label(root,text = "Pressure",font = ("Helvetica",11),fg = "white",bg = "#203243");
label3.place(x = 50,y = 160);

label4 = Label(root,text = "Wind Speed",font = ("Helvetica",11),fg = "white",bg = "#203243");
label4.place(x = 50,y = 180);

label5 = Label(root,text = "Desceiption",font = ("Helvetica",11),fg = "white",bg = "#203243");
label5.place(x = 50,y = 200);

#after above that labels will get created

##Search box

Search_image = PhotoImage(file = "Image from assets");
myimage = Label(image = Search_image,bg = "#57adff");
myimage.place(x = 270, y = 120);
#After that we will see search box on the screen

weat_image = PhotoImage(file = "Image from assets");
weatherimage = Label(root,image=weat_image,bg="#203243");
weatherimage.place(x = 290, y = 127);

textfield = tk.Entry(root,justify="center",width=15,font=("poppins",25,"bold"),bg="#203242",border=0,fg="white");
textfield.place(x = 370,y = 130);

search_icon = PhotoImage(file = "Images from assets");
myimage_icon = Button(image=search_icon,borderwidth=0,cursor="hand2",bg = "#203243",command=getWeather);
myimage_icon.place(x = 645,y = 125);


frame_ = Frame(root,width=900,height=180,bg = "#212120");
frame_.pack(side=BOTTOM);

#bottom boxes

firstbox = PhotoImage(file = "Images from assets");
secondbox = PhotoImage(file = "Iamge from assets of rounded rectanlges");

Label(frame_,image = firstbox,bg = "#212120").place(x = 30,y = 20);
Label(frame_,image = secondbox,bg = "#212120").place(x = 300,y = 30);
Label(frame_,image = secondbox,bg = "#212120").place(x = 400,y = 30);
Label(frame_,image = secondbox,bg = "#212120").place(x = 500,y = 30);
Label(frame_,image = secondbox,bg = "#212120").place(x = 600,y = 30);
Label(frame_,image = secondbox,bg = "#212120").place(x = 700,y = 30);
Label(frame_,image = secondbox,bg = "#212120").place(x = 800,y = 30);

#clock(here we will place time)
clock = Label(root,font = ("Helvetica",20),fg="white",bg = "#57adff");
clock.place(x = 30,y = 20);

long_lat = Label(root,font = ("Helvetica",20),fg="white",bg = "#57adff");
long_lat.place(x = 700,y = 50);


#thpwd
t = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t.place(x = 150,y = 120);

t1 = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t1.place(x = 150,y = 140);

t2 = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t2.place(x = 150,y = 160);

t3 = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t3.place(x = 150,y = 180);

t4 = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t4.place(x = 150,y = 200);

t5 = Label(root,font = ("Helvetica",11),fg="white",bg="#203243");
t5.place(x = 150,y = 200);






#timezone





























root.mainloop();
