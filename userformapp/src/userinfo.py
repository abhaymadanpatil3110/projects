import sys
from PyQt5.QtWidgets import QRadioButton,QApplication, QWidget, QLabel, QFileDialog, QLineEdit, QVBoxLayout, QFormLayout,QPushButton,QMessageBox
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette, QColor,QFont,QPixmap 

class UserForm(QWidget):
    
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("User Profile Form")
        self.setGeometry(500,300,500,300)
        self.layout = QVBoxLayout()
        self.setLayout(self.layout)
        
        self.form_layout = QFormLayout()
        self.layout.addLayout(self.form_layout) 
        
        self.label = QLabel("User Profile Form")
        font = QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.form_layout.addWidget(self.label)
        
        self.line1 = QLineEdit()
        self.line2 = QLineEdit()
        self.line3 = QLineEdit()
        self.line4 = QLineEdit()
        self.line5 = QLineEdit()
        
        self.form_layout.addRow("First Name: ",self.line1)
        self.form_layout.addRow("Last Name: ", self.line2)
        self.form_layout.addRow("Mobile No: ", self.line3)
        
        self.label_photo = QLabel("Selected Profile Photo: No file selected",None)
        #self.label_photo.setFixedSize(100,100)
        self.form_layout.addRow(self.label_photo)
        
        self.button1 = QPushButton("Select Photo")
        self.form_layout.addWidget(self.button1) 
        self.button1.clicked.connect(self.file_path)
        
        # create a radio button to select male or female
        self.label2 = QLabel("Gender:")
        self.form_layout.addRow(self.label2)
        
        self.radiobutton1 = QRadioButton("Male")
        self.form_layout.addWidget(self.radiobutton1)
        self.radiobutton1.clicked.connect(self.button_clicked)
        
        self.radiobutton2 = QRadioButton("Female")
        self.form_layout.addWidget(self.radiobutton2)
        self.radiobutton2.clicked.connect(self.button_clicked)
        
        self.form_layout.addRow("Height (cm):",self.line4)
               
        # create a button to submit the form
        self.button2 = QPushButton("Submit Form")
        self.layout.addWidget(self.button2)
        self.button2.clicked.connect(self.button_clicked)
        
    def file_path(self):
        path = QFileDialog.Option()
        #path = QFileDialog.DontUseNativeDialog
        image_path,_ = QFileDialog.getOpenFileName(self, 'Choose Profile Photo', '', 'Image Files (.png *.jpg *.jpeg *.bmp);;All Files ()', options= path )
        
        #if image_path:
         #   pixmap = QPixmap(image_path)
          #  self.label_photo.setPixmap(pixmap.scaled(200,200,aspectRatioMode=2))
                       
        
    def button_clicked(self):
        self.firstName = self.line1.text()
        self.lastName = self.line2.text()
        self.MobNo = self.line3.text()        
        self.male = self.radiobutton1.text()
        self.female = self.radiobutton2.text()
        self.Height = self.line4.text()
        
        if not (self.firstName and self.lastName and self.MobNo and self.male and self.female):
            self.msgError("Please fill all required field")
            return
        
        if not self.MobNo.isdigit() or len(self.MobNo)!=10:
            self.msgError("Please entrer valid mobile number")
            return
        
        self.dispData(f"User Profile Form Submitted \nFirst Name = {self.firstName} \nLast Name = {self.lastName} \nMobile No. = {self.MobNo} \nGneder = {self.male or self.female} \nHeight = {self.Height}\n Image Path = {self.file_path}")
        
    
    def msgError(self,message):  
        QMessageBox.critical(self,"Error",message,QMessageBox.Ok)   
        
    def dispData(self,message):
        QMessageBox.information(self,"Form Submitted",message,QMessageBox.Ok)
        
         
        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = UserForm()
    widget.show()
    sys.exit(app.exec_())
        